#include "../include/hmi.h"
#include <thread>
#include <chrono>

HMI::HMI()
{
    QFont font ("arial");
    font.setPixelSize(30);
    LabelUdater=std::make_unique<QTimer>(this);
    LabelUdater->setInterval(1000);
    connect(LabelUdater.get(),SIGNAL(timeout()),this,SLOT(UpdateFrequencyLabel()));
    QTimer * statusupdater = new QTimer(this);
    statusupdater->setInterval(1000);
    connect(statusupdater,SIGNAL(timeout()),this,SLOT(updateStatusLabel()));



    LabelUdater->start();
    Elements=std::make_unique<QGraphicsScene>();
    Elements->setSceneRect(0,0,800,600);
    MuteRightButton=std::make_unique<QPushButton>();
    MuteRightButton->setText(QString("MR OFF"));
    MuteRightButton->setGeometry(30,80,80,20);
    MuteRightButton->setFocusPolicy(Qt::NoFocus);
    MuteRightButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(MuteRightButton.get());
    MuteLeftButton=std::make_unique<QPushButton>();
    MuteLeftButton->setText(QString("ML OFF"));
    MuteLeftButton->setGeometry(30,120,80,20);
    MuteLeftButton->setFocusPolicy(Qt::NoFocus);
    MuteLeftButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(MuteLeftButton.get());
    StandbyButton=std::make_unique<QPushButton>();
    StandbyButton->setText(QString("STANDBY"));
    StandbyButton->setGeometry(30,160,80,20);
    StandbyButton->setFocusPolicy(Qt::NoFocus);
    StandbyButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(StandbyButton.get());
    StationFoundLabel = std::make_unique <QLabel> ();
    StationFoundLabel->setText(QString("Waiting"));
    StationFoundLabel->setGeometry(520,80,120,30);
    Elements->addWidget(StationFoundLabel.get());
    FMSet=std::make_unique<QLineEdit>();
    FMSet->setText(QString("Set own frequency here"));
    FMSet->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    FMSet->setValidator(new QIntValidator(880,1080,this));
    FMSet->setGeometry(200,20,300,20);
    Elements->addWidget(FMSet.get());
    MuteButton=std::make_unique<QPushButton>();
    SetFrequencyButton=std::make_unique<QPushButton>();
    SetFrequencyButton->setGeometry(200,60,300,30);
    SetFrequencyButton->setText(QString("SET FREQ"));
    Elements->addWidget(SetFrequencyButton.get());
    DecreaseFrequencyButton=std::make_unique<QPushButton>();
    DecreaseFrequencyButton->setGeometry(130,20,50,50);
    DecreaseFrequencyButton->setText(QString("-"));
    DecreaseFrequencyButton->setFont(font);
    DecreaseFrequencyButton->setFocusPolicy(Qt::NoFocus);
    DecreaseFrequencyButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(DecreaseFrequencyButton.get());
    IncreaseFrequencyButton=std::make_unique<QPushButton>();
    IncreaseFrequencyButton->setGeometry(520,20,50,50);
    IncreaseFrequencyButton->setText(QString("+"));
    IncreaseFrequencyButton->setFont(font);
    IncreaseFrequencyButton->setFocusPolicy(Qt::NoFocus);
    IncreaseFrequencyButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(IncreaseFrequencyButton.get());
    MuteButton->setGeometry(590,20,50,50);
    MuteButton->setText(QString("MUTE"));
    MuteButton->setFocusPolicy(Qt::NoFocus);
    MuteButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(MuteButton.get());
    SeekUpButton=std::make_unique<QPushButton>();
    SeekUpButton->setGeometry(30,20,90,20);
    SeekUpButton->setText(QString("Seek Up"));
    SeekUpButton->setFocusPolicy(Qt::NoFocus);
    SeekUpButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(SeekUpButton.get());
    SeekDownButton=std::make_unique<QPushButton>();
    SeekDownButton->setGeometry(30,50,90,20);
    SeekDownButton->setText(QString("Seek Down"));
    SeekDownButton->setFocusPolicy(Qt::NoFocus);
    SeekDownButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(SeekDownButton.get());
    setScene(Elements.get());
    setFixedSize(static_cast<int>(Elements->width()),static_cast<int>(Elements->height()));
    setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    MyRadio = std::make_unique<Radio>();
    MyRadio->setFrequency(1067);
    connect(MuteButton.get(),SIGNAL(released()),this,SLOT(Mute()));
    connect(SetFrequencyButton.get(),SIGNAL(released()),this,SLOT(ChangeFrequency()));
    connect(IncreaseFrequencyButton.get(),SIGNAL(released()),this,SLOT(IncreaseFrequencyText()));
    connect(DecreaseFrequencyButton.get(),SIGNAL(released()),this,SLOT(DecreaseFrequencyText()));
    connect(SeekUpButton.get(),SIGNAL(released()),this,SLOT(SearchUp()));
    connect(SeekDownButton.get(),SIGNAL(released()),this,SLOT(SearchDown()));
    connect(MuteLeftButton.get(),SIGNAL(released()),this,SLOT(MuteRight()));
    connect(MuteRightButton.get(),SIGNAL(released()),this,SLOT(MuteLeft()));
    connect(StandbyButton.get(),SIGNAL(released()),this,SLOT(Standby()));

    //  LabelUdater->start();
    show();
    statusupdater->start();
}

void HMI::Mute()
{
 MyRadio->switchMute();
 if (MuteButton->text()=="MUTE")
 {
     MuteButton->setText("UNMUTE");
 }
 else
 {
     MuteButton->setText("MUTE");

 }
 MyRadio->writeToRadio();


}

void HMI::MuteRight()
{
 MyRadio->switchMuteRight();
    if (MuteRightButton->text()=="MR ON")
    {
        MuteRightButton->setText("MR OFF");
    }
    else
    {
        MuteRightButton->setText("MR OFF");

    }

}

void HMI::MuteLeft()
{

   MyRadio->switchMuteLeft();

    if (MuteLeftButton->text()=="ML ON")
    {
        MuteLeftButton->setText("ML OFF");
    }
    else
    {
        MuteLeftButton->setText("ML OFF");

    }
}

void HMI::Standby()
{
    MyRadio->switchStandby();

    if (StandbyButton->text()=="STANDBY")
    {
        StandbyButton->setText("TURN ON");
    }
    else
    {
        StandbyButton->setText("STANDBY");


    }

}

void HMI::ChangeFrequency()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
    }

    MyRadio->setFrequency(FMSet->text().toInt());


}

void HMI::IncreaseFrequencyText()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
        return;
    }
    if (FMSet->text().toInt()>=1080)
    {
        return;
    }
    int newfreq= FMSet->text().toInt()+1;
    FMSet->setText(QString::number(newfreq));
}

void HMI::DecreaseFrequencyText()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
        return;
    }

    if (FMSet->text().toInt()<=880)
    {
        return;
    }

    int newfreq= FMSet->text().toInt()-1;
    FMSet->setText(QString::number(newfreq));
}

void HMI::UpdateFrequencyLabel()
{

    if(MyRadio->getFrequency()!=LastFrequency)
    {
    LastFrequency=MyRadio->getFrequency();
    FMSet->setText(QString::number(LastFrequency));
    }
   // StationCheck();

}

void HMI::SearchUp()
{

    StationFoundLabel->setText("SEARCHING");

    MyRadio->SearchUp();

    auto end = clock()+ 1 * CLOCKS_PER_SEC;
    while(clock()< end)
    {

    }

    if(MyRadio->isSearching())
    {
    StationFoundLabel->setText("FOUND RADIO");
    MyRadio->SearchUp();
    }

    else
    {
    StationFoundLabel->setText("NOT FOUND RADIO");
    MyRadio->SearchUp();
    }



}

void HMI::SearchDown()
{


    StationFoundLabel->setText("SEARCHING");

    MyRadio->SearchDown();

    auto end = clock()+ 1 * CLOCKS_PER_SEC;
    while(clock()< end)
    {

    }

    if(MyRadio->isSearching())
    {
    MyRadio->SearchDown();
    StationFoundLabel->setText("FOUND RADIO");
    }
    else
    {
    MyRadio->SearchDown();
    StationFoundLabel->setText("NOT FOUND");

    }

}

void HMI::StationCheck()
{



}

void HMI::updateStatusLabel()
{

    if (MyRadio->isStationPLaying())
    {

        StationFoundLabel->setText(QString("PLAYING"));


    }
    else
    {
        StationFoundLabel->setText(QString("NO STATION"));
    }



}


