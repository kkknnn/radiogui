#include "../include/radio.h"
#include <chrono>
void Radio::readFromRadio()
{

    int fd = wiringPiI2CSetup(READADRESS);
    FILE* fp = fdopen(fd, "r");
    fread(readmessage,1,5,fp);
}

int Radio::getFrequency()
{
    readFromRadio();
    int temp = readmessage[0]&0x3F;
    temp<<=8;
    temp|=readmessage[1];
    return (int)(((temp*32768)/4)-225000/100000)/100000-2;


}

void Radio::SearchUp()
{

//   uint freq = getFrequency();
//   qDebug() << freq;
//   uint temp=4*(freq*100000+225000)/32768;
//   uint16_t frequencyHighSideInjection=temp>>8;
//   uint16_t frequencyLowSideInjection=temp & 0xFF;
//   message[0]=frequencyHighSideInjection;
    // message[1]=frequencyLowSideInjection;


   qDebug() << "clicked " <<  message[0] << '\n';
    message[0]^=0x40;
   message[2]=0xD0;
   message[3]=0x1F;
   message[4]=0x00;
   qDebug() << "clicked " <<  message[0] << '\n';

   writeToRadio();

}

bool Radio::isSearching()
{
    readFromRadio();

    return (((int)readmessage[0]) & (1<<7));


}

bool Radio::isStationReg()
{
    readFromRadio();

    return (((int)readmessage[0]) & (1<<6));
}

bool Radio::isStationPLaying()
{
    readFromRadio();
    uint8_t temp =readmessage[3]>>4;

    if(temp>=14)
    return true;
    else
    {
    return false;
    }


}

void Radio::SearchDown()
{

    message[0]^=0x40;
    message[2]=0x50;
    message[3]=0x1F;
    message[4]=0x00;
    writeToRadio();


}

uint8_t Radio::getMessageByte(const int num) const
{
    return message[num];
}

void Radio::setFrequency(uint freq)
{
    uint temp=4*(freq*100000+225000)/32768;
    uint16_t frequencyHighSideInjection=temp>>8;
    uint16_t frequencyLowSideInjection=temp & 0xFF;
  //  std::cout << "Wybrana czestotliosc to " << (freq/10) << "," << (freq%10);
    message[0]=frequencyHighSideInjection;
    message[1]=frequencyLowSideInjection;
    message[2]=0xB0; //3 byte (0xB0): high side LO injection is on,.
    message[3]=0x10; //4 byte (0x10) : Xtal is 32.768 kHz
    message[4]=0x00; //5 byte0x00)
    writeToRadio();

}

void Radio::writeToRadio()
{

    int fd = wiringPiI2CSetup(WRITEADRESS);
    FILE* fp = fdopen(fd, "w");
    std::fwrite(message,1,5,fp);
    fclose(fp);
}

void Radio::switchMute()
{
    uint8_t mask =0x80;
    message[0]^=mask;
    writeToRadio();

}

void Radio::switchMuteLeft()
{
    uint8_t mask =0x02;
    message[2]^=mask;
    writeToRadio();

}

void Radio::switchMuteRight()
{
    uint8_t mask =0x04;
    message[2]^=mask;
    writeToRadio();
}

void Radio::switchStandby()
{
    uint8_t mask =0x40;
    message[3]^=mask;
    writeToRadio();
}
