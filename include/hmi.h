#ifndef HMI_H
#define HMI_H

#include <QGraphicsView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <memory>
#include <QValidator>
#include "radio.h"
#include <QTimer>
#include <QDebug>

class HMI : public QGraphicsView
{
    Q_OBJECT
public:
    HMI();
    ~HMI() = default;
public slots:
    void Mute();
    void MuteRight();
    void MuteLeft();
    void Standby();
    void ChangeFrequency();
    void IncreaseFrequencyText();
    void DecreaseFrequencyText();
    void UpdateFrequencyLabel();
    void SearchUp();
    void SearchDown();
    void StationCheck();
    void updateStatusLabel();

private:
    std::unique_ptr<QGraphicsScene> Elements;
    std::unique_ptr<QLineEdit> FMSet;
    std::unique_ptr<QPushButton> SetFrequencyButton;
    std::unique_ptr<QPushButton> DecreaseFrequencyButton;
    std::unique_ptr<QPushButton> IncreaseFrequencyButton;
    std::unique_ptr<QPushButton> MuteButton;
    std::unique_ptr<QPushButton> SeekUpButton;
    std::unique_ptr<QPushButton> SeekDownButton;
    std::unique_ptr<QPushButton> MuteRightButton;
    std::unique_ptr<QPushButton> MuteLeftButton;
    std::unique_ptr<QPushButton> StandbyButton;
    std::unique_ptr<QLabel> StationFoundLabel;
    int LastFrequency{0};

    std::unique_ptr<Radio> MyRadio;
    std::unique_ptr<QTimer> LabelUdater;
};


#endif // HMI_H
