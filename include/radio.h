#ifndef RADIO_H
#define RADIO_H
#include <iostream>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <linux/i2c-dev.h>
#include <QDebug>




class Radio
{
#define WRITEADRESS 0x60
#define READADRESS 0x60
#define READADRESSREG 0x10
public:
    Radio()=default;
    ~Radio()=default;
    void setFrequency(uint freq);
    void switchMute();
    void switchMuteLeft();
    void switchMuteRight();
    void switchStandby();

    void writeToRadio();
    void readFromRadio();
    int getFrequency();
    void SearchUp();
    bool isSearching();
    bool isStationReg();
    bool isStationPLaying();
    void SearchDown();
    uint8_t getMessageByte(const int num) const;
private:
    uint8_t message[5]={0x00};
    uint8_t readmessage[5]={0x00};
};


#endif // RADIO_H
